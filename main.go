package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/rcjuarez/examen_go/models"
)

func main() {
	var persDefecto models.Persona
	persDefecto = models.PersonaDefault()
	fmt.Println(" V A L O R E S  P O R  D E F E C T O")
	fmt.Println(persDefecto)
	fmt.Println(strings.Repeat("=", 100))

	nuevaPersona := models.PersonaParameters(31, "Ramón Cruz Juárez0", "M", 66.5, 160)
	fmt.Println(nuevaPersona)
	fmt.Println("V A L O R E S  P O R  P A R A M E T R O S")
	fmt.Println(nuevaPersona.CalcularIMC())
	fmt.Println(nuevaPersona.MayorEdad())
	fmt.Println(nuevaPersona.ToString())
	fmt.Println(strings.Repeat("=", 100))

	//Solictar información por pantalla
	n := solicitarNombre()
	//fmt.Println("Nombre ingresado: ", n)

	s := solicitarGenero()
	//fmt.Println("Ingrese su género: ", s)

	e := solicitarEdad()
	//fmt.Println("Su edad es: ", e)

	p := solicitarPeso()
	//fmt.Printf("El peso ingresado es : %f \n", p)

	a := solicitarEstatura()
	//fmt.Printf("La altura ingresada es : %f \n", a)
	fmt.Println("Termnando de solicitar la información...")

	otraPersona := models.PersonaParameters(e, n, s, p, a)
	otraPersona.SetSexo(s)
	imc := otraPersona.CalcularIMC()
	mje := ""

	switch imc {
	case 1:
		mje = " (Tiene sobrepeso)"
		break
	case 0:
		mje = " (Su peso es ideal)"
		break
	case -1:
		mje = " (está bajo de peso)"
		break
	}

	fmt.Println(strings.Repeat("=", 100))

	fmt.Println("Su índice de masa corporal es: ", imc, mje)
	if otraPersona.MayorEdad() {
		fmt.Println("Usted es mayor de edad")
	} else {
		fmt.Println("Tu eres menor de edad")
	}

	fmt.Printf("Información completa: %+v", otraPersona.ToString())
	fmt.Println("Nombre completo: ", otraPersona.GetNombre())
	fmt.Println("Edad: ", otraPersona.GetEdad())
	fmt.Println("Sexo: ", otraPersona.GetSexo())
	fmt.Println("Estatura: ", otraPersona.GetAltura())
}

func solicitarNombre() (n string) {
	fmt.Println("Ingrese su nombre completo : ")
	reader := bufio.NewReader(os.Stdin)
	// ReadString will block until the delimiter is entered
	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occured while reading input. Please try again", err)
		return
	}

	// remove the delimeter from the string
	return strings.TrimSuffix(input, "\n")
}

func solicitarGenero() (n string) {
	fmt.Println("Ingrese su género Hombre(H), Mujer(M) : ")
	reader := bufio.NewReader(os.Stdin)
	// ReadString will block until the delimiter is entered
	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occured while reading input. Please try again", err)
		return
	}

	// remove the delimeter from the string
	return strings.TrimSuffix(input, "\n")
}

func solicitarEdad() (e int) {
	fmt.Println("Ingrese su edad ([en años cumplidos sin decimales]) : ")
	_, err := fmt.Scanln(&e)
	if err != nil {
		fmt.Println(err)
	}
	return
}

func solicitarPeso() (p float64) {
	fmt.Println("Ingrese su peso ([en años kilogramos con decimales]) : ")
	_, err := fmt.Scanln(&p)
	if err != nil {
		fmt.Println(err)
	}
	return
}

func solicitarEstatura() (p float64) {
	fmt.Println("Ingrese su estatura ([en centímetros sin decimales]) : ")
	_, err := fmt.Scan(&p)
	if err != nil {
		fmt.Println(err)
	}
	return
}
