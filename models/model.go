package models

import (
	"math/rand"
)

const (
	pesoBajo          = -1
	pesoIdeal         = 0
	sobrePeso         = 1
	sexoDefault       = "H"
	mayorEdad         = 18
	lettersAndNumbers = "abcdefghijklmnopqrstuvwxyzABCEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

type Persona struct {
	nombre   string  //Nombre completo
	edad     int     //Número de años enteros
	nss      string  //Número de seguridad social
	sexo     string  //H o M
	pesoKg   float64 // Peso en kilogramos
	alturaCM float64 //Alturas en centímetros
}

func PersonaDefault() Persona {
	return Persona{"", 0, generaNSS(), sexoDefault, 0.0, 0.0}
}

func PersonaParameters(edad int, nombre, sex string, pesokg, alturacm float64) Persona {
	return Persona{nombre, edad, generaNSS(), comprobarSexo(sex), pesokg, alturacm}
}

func generaNSS() string {
	numCarac := 8
	bytes := make([]byte, numCarac)

	for i := 0; i < numCarac; i++ {
		bytes[i] = lettersAndNumbers[rand.Intn(len(lettersAndNumbers))]
	}

	return string(bytes)
}

func comprobarSexo(sx string) string {
	if len(sx) == 0 {
		return sexoDefault
	}
	if sx[0] == 109 || sx[0] == 77 {
		return "M"
	} else {
		return sexoDefault
	}
}
