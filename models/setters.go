package models

/*
	Funciones Set y Get
*/

//Nombre
func (p Persona) GetNombre() string { return p.nombre }

func (p Persona) SetNombre(nombre string) { p.nombre = nombre }

//Edad

func (p Persona) GetEdad() int { return p.edad }

func (p Persona) SetEdad(edad int) { p.edad = edad }

//Sexo

func (p *Persona) GetSexo() string { return p.sexo }

func (p Persona) SetSexo(sexo string) { p.sexo = comprobarSexo(sexo) }

//Peso
func (p Persona) GetPeso() float64 { return p.pesoKg }

func (p Persona) SetPeso(peso float64) { p.pesoKg = peso }

//Altura
func (p Persona) GetAltura() float64 { return p.alturaCM }

func (p Persona) SetAltura(altura float64) { p.alturaCM = altura }

//Funcionalidades

func (per Persona) CalcularIMC() int {
	//(peso en kg/(Estatura^2 en m))
	mts := per.GetAltura() / 100
	peso := per.GetPeso() / (mts * mts)

	condicionH := 20.0
	rangoSup := 5.0
	rangoInf := 1.0

	if per.GetSexo() == sexoDefault {
		if peso < condicionH {
			return pesoBajo
		} else if peso > condicionH+rangoSup {
			return sobrePeso
		} else {
			return pesoIdeal
		}
	} else {
		if peso < (condicionH - rangoInf) {
			return pesoBajo
		} else if peso > (condicionH-rangoInf)+rangoSup {
			return sobrePeso
		} else {
			return pesoIdeal
		}
	}
}

func (per Persona) MayorEdad() bool {
	if per.GetEdad() > mayorEdad {
		return true
	}
	return false
}

func (per Persona) ToString() Persona { return per }
